import React from 'react';
import logo from './logo.svg';
import './App.css';
import TransactionApi from './components/transactionApi'

function App() {
  return (
    <div className="App">
      <h1>Transactions</h1>
      <TransactionApi />
    </div>
  );
}
export default App;
