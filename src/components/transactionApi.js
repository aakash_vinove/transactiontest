import React, { Component } from 'react';
import TransactionUi from './transactionUi'
const rows = [
 { transactionId: 1, to: "Brad", from: "Joe", amount: 500 },
 { transactionId: 2, to: "Brad", from: "Joe", amount: 500 },
 { transactionId: 3, to: "Brad", from: "Joe", amount: 500 },
 { transactionId: 4, to: "Brad", from: "Joe", amount: 500 },
];
class TransactionApi extends Component {
 constructor(props) {
  super(props)
  this.state = {
   transaction: []
  }
 }
 componentDidMount() {
  this.setState({ transaction: rows })
 }
 render() {
  console.log(rows)
  return (
   <TransactionUi rows={this.state.transaction} />
  );
 }
}
export default TransactionApi;